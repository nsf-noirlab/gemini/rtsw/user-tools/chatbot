import streamlit as st 
import tempfile
from dotenv import load_dotenv
from PyPDF2 import PdfReader
from langchain_community.document_loaders.csv_loader import CSVLoader
from langchain.text_splitter import CharacterTextSplitter
from langchain_community.embeddings import HuggingFaceInstructEmbeddings
from langchain_community.vectorstores import FAISS
from langchain_community.llms import Ollama
from langchain_community.callbacks import StreamlitCallbackHandler
from langchain.memory import ConversationBufferMemory
from langchain.chains import ConversationalRetrievalChain
from htmlTemplate import *


def load_csv(documents):
    with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
        tmp_file.write(documents.getvalue())
        tmp_path = tmp_file.name

    reader = CSVLoader(
            file_path=tmp_path,
            encoding="utf-8",
            csv_args={
                "delimiter": ",",
                }
            )
    data = reader.load()
    return data

def get_csv_chunks(raw_csv):
    text_splitter = CharacterTextSplitter(
        separator="\n",
        chunk_size=2000,
        chunk_overlap=200,
        length_function=len
    )
    chunks = text_splitter.split_documents(raw_csv)
    return chunks


def get_raw_text(documents):
    
    text = ""
    reader = PdfReader(documents)
    for page in reader.pages:
        text += page.extract_text()

    return text

def get_text_chunks(raw_text):
    
    text_splitter = CharacterTextSplitter(
        separator="\n",
        chunk_size=1000,
        chunk_overlap=200,
        length_function=len
    )
    chunks = text_splitter.split_text(raw_text)
    return chunks

def get_vector_store(chunks,file_type):
    embeddings = HuggingFaceInstructEmbeddings(model_name='hkunlp/instructor-xl')
    
    if file_type == "text/csv":
        vectorstore = FAISS.from_documents(chunks, embedding=embeddings)
    else:
        vectorstore = FAISS.from_texts(chunks, embedding=embeddings)
    return vectorstore

def get_conversation_chain(vectorstore):
    llm = Ollama(model="llama3:70b",
             temperature = 0.9,
             verbose=True
            )
    memory = ConversationBufferMemory(memory_key='chat_history', return_messages=True)
    st_callback = StreamlitCallbackHandler(st.empty())
    conversation = ConversationalRetrievalChain.from_llm(
        llm=llm,
        retriever=vectorstore.as_retriever(),
        memory=memory,
        callbacks=[st_callback]
    )
    return conversation

def handle_input(question):
    if st.session_state.conversation is None:
        st.warning("Please upload and process documents before asking a question.")
        return

    response = st.session_state.conversation({"question": question})
    st.session_state.chat_history = response['chat_history']
    for i, message in enumerate(st.session_state.chat_history):
        if i % 2 != 0:
            st.write(bot_template.replace("{{MSG}}", message.content), unsafe_allow_html=True)
        else:
            st.write(user_template.replace("{{MSG}}", message.content), unsafe_allow_html=True)
            
def main():
    load_dotenv()
    st.set_page_config(page_title="Fault chatbot", page_icon=":hammer_and_wrench:")
    st.write(css, unsafe_allow_html=True)
    if "conversation" not in st.session_state:
        st.session_state.conversation = None
    
    if "chat_history" not in st.session_state:
        st.session_state.chat_history = None

    st.header("Fault chatbot :hammer_and_wrench:")
    question = st.text_input("Ask a question about your recent fault")

    if question:
        handle_input(question)

    with st.sidebar:
        st.subheader("Your documents")
        documents = st.file_uploader("Upload your documents and click on Process")
        if st.button("Process"):
            with st.spinner("Processing"):
                if documents.type == "text/csv":
                    raw_csv = load_csv(documents)
                    chunks = get_csv_chunks(raw_csv)
                else:
                    raw_text = get_raw_text(documents)
                    
                    chunks = get_text_chunks(raw_text)

                vectorstore = get_vector_store(chunks, documents.type)
                
                st.session_state.conversation = get_conversation_chain(vectorstore)

                st.write("Done! You may start asking questions :smile:")




if __name__ == '__main__':
    main()