# Chatbot Repository

Welcome to the Chatbot Repository! This repository contains the source code for the Fault chatbot project.

## Description

This chatbot is useful for getting details about your faults

## Installation

1. Clone the repository to your local machine:

```bash
git clone https://gitlab.com/nsf-noirlab/gemini/rtsw/user-tools/chatbot
```

2. Download the Ollama language model. Follow instructions under setup here[https://python.langchain.com/docs/integrations/llms/ollama#setup]



To run the chatbot, follow these steps:

1. Navigate to the project directory:
```bash
cd chatbot
```

2. Create a virtual environment. Make sure you have Python 3 installed. You can create a virtual environment using virtualenv or venv:
```bash
# Using virtualenv
virtualenv venv

# Using venv (Python 3.3+)
python -m venv venv
```

3. Install the required dependencies. Make sure you have Python 3 installed. You can install the dependencies using pip:
```bash
pip install -r requirements.txt
```

4. Execute the main script:
```bash
streamlit run app.py
```

Follow the on-screen instructions to interact with the chatbot.

# NOTE: CSV takes forever!